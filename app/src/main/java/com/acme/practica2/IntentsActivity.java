package com.acme.practica2;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import android.app.*;
import android.content.Intent;
import android.net.Uri;
import java.util.Locale;

import static android.content.Intent.*;


public class IntentsActivity extends ActionBarActivity {

    private EditText et;
    private Spinner spin;
    private String spinValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intents);

        spin=(Spinner)findViewById(R.id.spinner);
        et=(EditText)findViewById(R.id.editText);
        Button myButton=(Button)findViewById(R.id.buttonInt);


        // The Adapter is the link between the GUI and the data: the list is taken from "elementos" in array.xml
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.elementos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);

        // add the listener to the spinner
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ArrayAdapter<String> aa = (ArrayAdapter<String>) parent.getAdapter();

                spinValue=aa.getItem(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //add the listener to the button
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isTextFieldEmpty()){
                    showAlert();
                }else{

                    if(spinValue.equals("Phone dialer")){

                        Log.d("Test","Call");
                        Intent i = new Intent(ACTION_DIAL, Uri.parse("tel:"+et.getText().toString()));
                        startActivity(i);

                    }
                    if(spinValue.equals("Web browser")){

                        Log.d("Web",et.getText().toString());
                        String url=et.getText().toString();

                        if (!url.startsWith("http://") && !url.startsWith("https://")) {
                            url = "http://" + url;
                        }

                        Intent i = new Intent(ACTION_VIEW, Uri.parse(url));
                        startActivity(i);

                    }
                    if(spinValue.equals("Geolocation")){

                        Log.d("Web",et.getText().toString());
                        String city=et.getText().toString();

                        String url="https://www.google.com/maps/search/"+city;

                        Intent i = new Intent(ACTION_VIEW, Uri.parse(url));
                        startActivity(i);

                    }
                    if(spinValue.equals("Messaging")){

                        String number = et.getText().toString();  // The number on which you want to send SMS
                        startActivity(new Intent(Intent.ACTION_SEND, Uri.fromParts("sms", number, null)));

                    }

                }

            }
        });

    }

    // This method is used to check if the textField is empty or not
    private boolean isTextFieldEmpty(){

        if(et.getText().toString()==null){
            return true;
        }else{
            return false;
        }

    }

    // if the user click "GO" without fill the form this alert appears
    private void showAlert(){

        new AlertDialog.Builder(IntentsActivity.this)
                .setTitle("Attention")
                .setMessage("You must fill the form!").show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_intents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
